<?php

namespace Holaluz;

class Functions
{

    public static function startsWith($haystack, $needle)
    {
        return $needle === '' || strpos($haystack, $needle) === 0;
    }

    public static function endsWith($haystack, $needle)
    {
        return $needle === '' || substr($haystack, -strlen($needle)) === $needle;
    }

    public static function randomString($length = 32, $chars = '1234567890abcdef')
    {
        $charsLength = (strlen($chars) - 1);
        $string = $chars{rand(0, $charsLength)};

        for ($counter = 1; $counter < $length; $counter = strlen($string)) {
            $repeat = $chars{rand(0, $charsLength)};

            if ($repeat != $string{$counter - 1}) {
                $string .=  $repeat;
            } else {
                --$counter;
            }
        }

        return $string;
    }

    /**Converts string from underscore to camel case **/
    public static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }

    public static function camelCaseToUnderscore($string)
    {
        return substr(strtolower( preg_replace( '/([A-Z])/', '_$1', $string)),1);
    }

    public static function toBoolean($string)
    {
        return filter_var($string, FILTER_VALIDATE_BOOLEAN);
    }

    public static function to_boolean($string)
    {
        return toBoolean($string);
    }

    public static function _print_r($msg)
    {
        echo "<pre>";
        print_r($msg);
        echo "</pre>";
    }

    public static function europeanDate($mysqlDate)
    {
        return date("d/m/Y", strtotime($mysqlDate));
    }

    public static function europeanDateTime($mysqlDate)
    {
        return date("d/m/Y H:m:s", strtotime($mysqlDate));
    }

    public static function convertToKiloWatt($valueW)
    {
        return (float)$valueW/1000;
    }

    public static function convertToWatt($valuekW)
    {
        return (float)$valuekW*1000;
    }

    public static function comaToDot($value)
    {
        return str_replace(',', '.', $value);
    }

    public static function dotToComa($value)
    {
        return str_replace('.', ',', $value);
    }

    public static function nameToParts($fullname)
    {
        if (strpos($fullname, ',') !== false) {
            $parts = explode(',', $fullname);
            $firstname = $parts[1];
            $secondnames = explode(' ', $parts[0]);
            $lastname1 = $secondnames[0];
            $lastname2 = '';
            if (count($secondnames) > 1) {
                for ($i = 1; $i < count($secondnames); $i++) {
                    $lastname2 .= $secondnames[$i] . ' ';
                }

                $lastname2 = trim($lastname2);
                }
            } else {
                $nameparts = explode(' ', $fullname);
                if (count($nameparts) == 1) {
                    $firstname = $nameparts[0];
                    $lastname1 = '';
                    $lastname2 = '';
                } elseif (count($nameparts) == 2) {
                    $firstname = $nameparts[0];
                    $lastname1 = $nameparts[1];
                    $lastname2 = '';
                } elseif (count($nameparts) == 3) {
                    $firstname = $nameparts[0];
                    $lastname1 = $nameparts[1];
                    $lastname2 = $nameparts[2];
                } elseif (count($nameparts) == 4) {
                    $firstname = $nameparts[0] . ' ' . $nameparts[1];
                    $lastname1 = $nameparts[2];
                    $lastname2 = $nameparts[3];
                } else {
                    $firstname = $nameparts[0] . ' ' . $nameparts[1];
                    $lastname1 = $nameparts[2];
                    $lastname2 = '';
                    for ($i = 3; $i < count($nameparts); $i++) {
                        $lastname2 .= $nameparts[$i];
                    }
                }
            }

            $firstname = trim($firstname);
            $lastname1 = trim($lastname1);
            $lastname2 = trim($lastname2);

            return compact('firstname', 'lastname1', 'lastname2');
    }

    /**
     * Returns key of an element of an array (which is a array map itself), based on $callback condition
     * @param $arrayOfMaps - array of map arrays
     * @param $searchValue
     * @param $callback - function which sets the condition to return the right value
     */
    public static function array_deep_find_key($arrayOfMaps, $callback)
    {
        foreach ($arrayOfMaps as $key => $map) {
            if ($callback($map))
                return $key;
        }
    }

    /** Same as previous but returns element * */
    public static function array_deep_find($arrayOfMaps, $callback)
    {
        foreach ($arrayOfMaps as $key => $map) {
            if ($callback($map))
                return $arrayOfMaps[$key];
        }
    }

    /**
     * Alter all keys of array by adding prefix or postfix to them
     * @param array $array
     * @param unknown $string
     * @param string $separator
     * @param string $prefix
     * @return array
     */
    public static function array_alter_keys(array $array, $callback)
    {
        $array = array_combine(
            array_map(function($k) use($callback) {
                return $callback($k);
            }, array_keys($array)), $array
        );

        return $array;
    }

    /** return subset of all elements of array which satisfy the condition **/
    public static function array_filter_by_keys($array, $callback)
    {
        $returnArray = array();
        foreach ($array as $key => $value) {
            if ($callback($key)) {
                $returnArray[$key] = $value;
            }
        }

        return $returnArray;
    }

    /** Returns only elements of array who have given keyes **/
    public static function array_subset_by_keys($array, $keys)
    {
        return self::array_filter_by_keys($array, function($key) use($keys){
            return in_array($key, $keys);
        });
    }

    /** Returns element of array without elements with given keys **/
    public static function array_remove_keys($array, $keys)
    {
        foreach($array as $key => $element) {
            if(in_array($key, $keys)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /** Converts list of arrays(or objects) to list of arrays containing only properties in $properties array
     */
    public static function list_subset_by_properties($list, array $properties)
    {
        $res = array();

        foreach ($list as $element) {
            if (is_object($element)) {
                $element = $element->toArray();
            }

            $res[] = self::array_subset_by_keys($element, $properties);
        }

        return $res;
    }

    public static function list_subset_by_property($list, $property)
    {
        $res = array();

        foreach ($list as $element) {
            if (is_object($element)) {
                $element = $element->toArray();
            }

            $res[] = $element[$property];
        }

        return $res;
    }

    public static function zerofill($num, $zerofill = 5)
    {
        return str_pad($num, $zerofill, '0', STR_PAD_LEFT);
    }

    public static function sortArrayByArray($array, $orderArray)
    {
        $ordered = array();

        foreach ($orderArray as $key) {
            if (array_key_exists($key, $array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
            }
        }
        return $ordered;
    }

    public static function processTemplate($text, $params)
    {
        foreach ($params as $key => $value) {
            $text = str_replace('${' . $key . '}', $value, $text);
        }

        return $text;
    }


    public static function slugify($string, $separator = '-', $toLower = true)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', $separator, $string);
        if ($toLower) {
            $slug = strtolower($slug);            
        }
        
        return $slug;
    }

    public static function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                    $key = 'item'.$key; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml_data->addChild($key);
                self::array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }
    }

    public static function xml_to_array($xmlstring) {
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }

    public static function xmlpath_to_array($xmlpath) {
        $xml = simplexml_load_file($xmlpath);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }

    public static function array_to_object($array, $deep = true)
    {
        if ($deep == true) {
          $obj = new \stdClass;
          foreach($array as $k => $v) {
             if(strlen($k)) {
                if(is_array($v)) {
                   $obj->{$k} = self::array_to_object($v); //RECURSION
                } else {
                   $obj->{$k} = $v;
                }
             }
          }

          return $obj;
      } else {
        return (object) $array;
      }
    }

    /**
     * @param  mixed $source        can be a folder, a file, or array of files
     * @param  string $destination  destination folder
     * @return boolean
     */
    public static function zip($source, $destination)
    {
        if (extension_loaded('zip')) {
                if ( (!is_array($source) && file_exists($source)) || file_exists($source[0])) {
                    $zip = new \ZipArchive();
                    if ($zip->open($destination, \ZIPARCHIVE::CREATE)) {

                        if (! is_array($source)) {
                            $source = realpath($source);
                            if (is_dir($source)) {
                                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);
                                foreach ($files as $file) {
                                    $file = realpath($file);
     
                                    // if (self::endsWith($file, [".gitignore", "."])) {
                                    //     continue;
                                    // }

                                    if (is_dir($file)) {
                                        $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                                    } else if (is_file($file)) {
                                        $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                                    }
                                }
                            } else if (is_file($source)) {
                                $zip->addFromString(basename($source), file_get_contents($source));
                            }
                        } else {
                            foreach ($source as $s) {
                                $s = realpath($s);
                                $zip->addFromString(basename($s), file_get_contents($s));
                            }
                        }
                    }
                return $zip->close();
            }
        }
            
        return false;
    }

    public static function unzip($source, $destination)
    {
        $zip = new \ZipArchive;
        $res = $zip->open($source);
        if ($res === TRUE) {
          $zip->extractTo($destination);
          $zip->close();
          return true;
        } else {
          return false;
        }
    }
}
