<?php

namespace Holaluz;

class Validators
{

    public static function validCif($value)
    { 
        if ($value=="") {
            return false;
        }
        
        //Elimina todos los espacios, y lo convierte en mayúsculas
        $value = preg_replace('/\s+/', '', $value);
        $value = strtoupper($value);
        
        //http://www.formacionlibre.com/blog/significado-de-la-letra-del-cif-o-nif-de-las-empresas
        //K,L,M are obsolete...
        $inicio = "ABCDEFGHJNPQRSUVW";
        
        //Comprobamos longitud
        if (strlen($value) != 9) {
            return false;
        }
        //Comprobamos primera letra
        else if (strpos ($inicio,substr($value,0,1)) === false)
        {
            return false;
        }
        else{            
            //1. Sumar digitos posiciones pares (A)
            $suma = $value[2] + $value[4] + $value[6];
                        
            //2. Para cada dígito posición impar, multiplicar * 2 y sumar resultado (B)
            //3. Sumar A+B = C
            for ($i = 1; $i < 8; $i += 2)
            {
                $suma += substr((2 * $value[$i]),0,1) + substr((2 * $value[$i]), 1, 1);
            }
            
            //4. Obtener Dígito D
            $n = 10 - substr($suma, strlen($suma) - 1, 1);
            
            //Comprovamos último dígito / letra
            //Si ha de ser numérico es directamente D y si se trata de una letra se corresponde con la relación:
            //J = 0, A = 1, B = 2, C= 3, D = 4, E = 5, F = 6, G = 7, H = 8, I = 9
            if ($value[8] == chr(64 + $n) || $value[8] == substr($n, strlen($n) - 1, 1))
            {
                return true;
            }
            else {
                return false;
            }
        }    
    }

    // Función auxiliar usada para CIFs y NIFs especiales
    private static function getCifSum($cif) {
        $sum = $cif[2] + $cif[4] + $cif[6];
 
        for ($i = 1; $i<8; $i += 2) {
            $tmp = (string) (2 * $cif[$i]);
 
            $tmp = $tmp[0] + ((strlen ($tmp) == 2) ?  $tmp[1] : 0);
 
            $sum += $tmp;
        }
 
        return $sum;
    }

    // Valida DNIs
    public static function validDni($nif) {
        $nif_codes = 'TRWAGMYFPDXBNJZSQVHLCKE';
  
        $sum = (string) self::getCifSum($nif);
        $n = 10 - substr($sum, -1);
 
        if (preg_match ('/^[0-9]{8}[A-Z]{1}$/', $nif)) {
            // DNIs
            $num = substr($nif, 0, 8);
 
            return ($nif[8] == $nif_codes[$num % 23]);
        }
 
        return false;
    }
 
    public static function validPassport($passport) {

        if(preg_match ('/^[0-9A-Za-z]{8,12}$/', $passport)) {
            return true;
        }
        return false;
    }

    // Valida NIFs y NIEs, con especiales.
    // De hecho, un DNI es también un NIF!!!
    public static function validNif($nif) {
        $nif_codes = 'TRWAGMYFPDXBNJZSQVHLCKE';
  
        $sum = (string) self::getCifSum ($nif);
        $n = 10 - substr($sum, -1);
 
        if (preg_match ('/^[0-9]{8}[A-Z]{1}$/', $nif)) {
            // DNIs
            $num = substr($nif, 0, 8);
 
            return ($nif[8] == $nif_codes[$num % 23]);
        } elseif (preg_match ('/^[KLM]{1}/', $nif)) {
            // NIFs especiales
            return ($nif[8] == chr($n + 64));
        }
 
        return false;
    }
    
    public static function validNie($nif) {
        $dni = strtoupper($nif);
 
        $letra = substr($dni, -1, 1);
        $numero = substr($dni, 0, 8);

        // Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
        $numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);   

        $modulo = $numero % 23;
        $letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
        $letra_correcta = substr($letras_validas, $modulo, 1);

        if($letra_correcta!=$letra) {
            return false;
        }
        else {
            return true;
        }
    }
}
